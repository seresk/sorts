/**
 * Created by seresk on 2017. 10. 03..
 */
var limit = 10;
var randomArray = [];
for (var i = 0; i < limit; i++) {
    randomArray[i] = Math.floor(Math.random() * 1000);

}
for (var i = randomArray.length - 1; i > 0; i--) {
    for (var j = 0; j < i; j++) {
        if (randomArray[j] > randomArray[j + 1]) {
            var tmp = randomArray[j + 1];
            randomArray[j + 1] = randomArray[j];
            randomArray[j] = tmp;
        }
    }

}

//Verification
var verified = true;
for (var i = 0; i < randomArray.length; i++) {
    if (randomArray[i] > randomArray[i + 1])
        verified = false;
}
console.log("Result: ", verified);