var randomArray = [];
var limit = 10;
for (var i = 0; i < limit; i++) {
    randomArray[i] = Math.floor(Math.random() * 1000);

}
console.log("Random:", randomArray);

for (var i = randomArray.length - 1; i >= 0; i--) {
    var finished = false;
    for (var j = i; j <= randomArray.length - 1 && finished == false; j++) {

        var next = randomArray[j + 1];
        var current = randomArray[j];
        if (j < randomArray.length && current > next) {

            const tmp = next;
            randomArray[j + 1] = current;
            randomArray[j] = tmp;

        } else {
            finished = true;
        }

    }

}

//Verification
var verified = true;
for (var i = 0; i < randomArray.length; i++) {
    if (randomArray[i] > randomArray[i + 1])
        verified = false;
}
console.log("Ordered:", randomArray, "Result: ", verified);