var limit = 10;
var r = [];
var subStackSize = 0;
for (var i = 0; i < limit; i++) {
    r[i] = Math.floor(Math.random() * 1000);

}
console.log("Random:", r);
/**
 * Actual implementation starts here
 * This one selects the smallest value from the reamaining random interval and puts it to teh start of the by switching
 * the start value with teh found value
 */
for (var i = 0; i < r.length; i++) {
    var smallest = null;
    var smallestIndex = null;
    for (var j = i; j < r.length; j++) {
        if (smallest == null) {
            smallest = r[j];
            smallestIndex = j;
            continue;
        }
        if (r[j] < smallest) {
            smallest = r[j];
            smallestIndex = j;
        }

    }
    var tmp = r[i];
    r[i] = smallest;
    r[smallestIndex] = tmp;
}

//Verification
var verified = true;
for (var i = 0; i < r.length; i++) {
    if (r[i] > r[i + 1])
        verified = false;
}
console.log("Ordered:", r, "Result: ", verified);