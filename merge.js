var sizeOfTheList = 10000;
var randomArray = [];
for (var i = 0; i < sizeOfTheList; i++) {
    randomArray[i] = Math.floor(Math.random() * 1000);

}
console.log("Array of randoms:", randomArray);
/**
 * Each sublist will contain one element at the beginning.
 */
subStackSize = 1;
/**
 *  This iteration will continue until there is only one sublist remaining, this will be the result.
 */
while (subStackSize <= sizeOfTheList) {
    /**
     * CopyPointer marks the index where we grab the sub lists
     */
    var copyPointerUpperLimit = 0;
    while (copyPointerUpperLimit <= sizeOfTheList) {
        /**
         * We fill the sub-lists
         */
        var lower = [];
        var upper = [];
        var copyPointer = 0 + copyPointerUpperLimit;
        for (; copyPointer < subStackSize + copyPointerUpperLimit; copyPointer++) {
            if (typeof randomArray[copyPointer] !== "undefined")
                lower.push(randomArray[copyPointer]);
        }
        if (typeof randomArray[copyPointer] !== "undefined")
            for (; copyPointer < Math.min(2 * subStackSize + copyPointerUpperLimit, sizeOfTheList); copyPointer++) {
                if (typeof randomArray[copyPointer] !== "undefined")
                    upper.push(randomArray[copyPointer]);
            }
        /**
         * We merge the sublists into an ordered list.
         */
        var lowerPointer = 0;
        var upperPointer = 0;
        var randomPointer = 0 + copyPointerUpperLimit;
        var orderedSubList = [];
        for (; randomPointer < Math.min(2 * subStackSize + copyPointerUpperLimit, sizeOfTheList); randomPointer++) {
            if (typeof lower[lowerPointer] !== "undefined" && typeof upper[upperPointer] !== "undefined") {
                orderedSubList[lowerPointer + upperPointer] = lower[lowerPointer] < upper[upperPointer] ? lower[lowerPointer++] : upper[upperPointer++];
            }

            if (typeof lower[lowerPointer] === "undefined" && typeof upper[upperPointer] !== "undefined") {
                orderedSubList[lowerPointer + upperPointer] = upper[upperPointer++];
            }

            if (typeof lower[lowerPointer] !== "undefined" && typeof upper[upperPointer] === "undefined") {
                orderedSubList[lowerPointer + upperPointer] = lower[lowerPointer++];
            }
        }
        /**
         * We copy the merged sorted list into the random array
         */
        for (var k = copyPointerUpperLimit; orderedSubList.length != 0; k++) {
            randomArray[k] = orderedSubList.shift();
        }
        /**
         * We increase the size of the sublists
         * @type {number}
         */
        copyPointerUpperLimit += 2 * subStackSize;
    }
    subStackSize = 2 * subStackSize
}
//Verification
var verified = true;
for (i = 0; i < randomArray.length; i++) {
    if (randomArray[i] > randomArray[i + 1])
        verified = false;
}
console.log("Ordered:", randomArray, "Result: ", verified);
