/**
 * Created by seresk on 2017. 10. 04..
 */
var randomArray = [];
var limit = 20;
for (var i = 0; i < limit; i++) {
    randomArray[i] = Math.floor(Math.random() * 1000);

}
console.log("Random:", randomArray);


var scope = Math.floor(randomArray.length / 2);

while (scope >= 1) {

    /**
     * Check forwards
     */
    for (var i = 0; i + scope <= randomArray.length - 1; i++) {
        if (randomArray[i] > randomArray[i + scope]) {

            var backwardIndex = i;

            var tmp = randomArray[i];
            randomArray[i] = randomArray[i + scope];
            randomArray[i + scope] = tmp;

            /**
             * Check backwards
             */
            while ((backwardIndex - scope) >= 0) {
                if (randomArray[backwardIndex - scope] > randomArray[backwardIndex]) {
                    console.log("B", backwardIndex, ": switching:", randomArray[backwardIndex - scope], ' > ', randomArray[backwardIndex], randomArray[backwardIndex - scope] > randomArray[backwardIndex])

                    var tmp = randomArray[backwardIndex];
                    randomArray[backwardIndex] = randomArray[backwardIndex - scope];
                    randomArray[backwardIndex - scope] = tmp;
                    backwardIndex = backwardIndex - scope;
                } else {
                    backwardIndex = -1;
                }
            }
        }

    }
    scope = Math.floor(scope / 2);
}

//Verification
var verified = true;
for (var i = 0; i < randomArray.length; i++) {
    if (randomArray[i] > randomArray[i + 1])
        verified = false;
}
console.log("Ordered:", randomArray, "Result: ", verified);