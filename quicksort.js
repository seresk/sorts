var randomArray = [];
var limit = 15;
for (var i = 0; i < limit; i++) {
    randomArray[i] = Math.floor(Math.random() * 1000);

}
console.log("Random:", randomArray);

function run(leftBoundIndex, rightBoundIndex) {

    if (leftBoundIndex < 0 || Math.abs(leftBoundIndex - rightBoundIndex) < 1)
        return;

    var pivotIndex = Math.floor((leftBoundIndex + rightBoundIndex) / 2);
    var pivotValue = randomArray[pivotIndex];


    /**
     * Allocating the lower than pivotValues to leftOfThePivot
     * Allocating the bigger than pivotValues to rightOfThePivot
     */

    var lessThanThePivot = [];
    var greaterThanThePivot = [];


    for (var i = leftBoundIndex; i <= rightBoundIndex; i++) {
        if (i === pivotIndex)
            continue;
        if (randomArray[i] <= randomArray[pivotIndex]) {
            lessThanThePivot.push(randomArray[i])
        } else {
            greaterThanThePivot.push(randomArray[i])
        }
    }

    /**
     * Overwriting the unsorted list with the {lessThanThePivot,pivotValue,greaterThanThePivot}
     */
    for (var i = leftBoundIndex, j = 0; i <= rightBoundIndex; i++, j++) {

        if (j <= lessThanThePivot.length - 1) {
            randomArray[i] = lessThanThePivot[j];
        }
        if (j === lessThanThePivot.length) {
            randomArray[i] = pivotValue;
            pivotIndex = i;
        }
        if (j > lessThanThePivot.length) {
            randomArray[i] = greaterThanThePivot[j - (lessThanThePivot.length + 1)];
        }

    }
    /**
     * Quicksort anything left of the pivot
     */
    if (lessThanThePivot.length >= 2) {
        run(pivotIndex - (lessThanThePivot.length), pivotIndex - 1);
    }

    /**
     * Quicksort anything right of the pivot
     */
    if (greaterThanThePivot.length >= 2) {
        run(pivotIndex + 1, pivotIndex + (greaterThanThePivot.length ));
    }

}

run(0, randomArray.length - 1);

//Verification
var verified = true;
for (var i = 0; i < randomArray.length; i++) {
    if (randomArray[i] > randomArray[i + 1])
        verified = false;
}

console.log("Ordered:", randomArray, "Result: ", verified);